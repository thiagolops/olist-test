from datetime import timedelta

import coreapi
import coreschema
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import schemas

from .models import Call
from .serializers import (CallBillSerializer, CallEndSerializer,
                          CallSerializer, CallStartSerializer)


class CallListView(generics.ListAPIView):
    """
    get: List all the calls.
    """
    queryset = Call.objects.all()
    serializer_class = CallSerializer


class CallRetrieve(generics.RetrieveDestroyAPIView):
    """
    get: Details of a call
    delete: Delete a call
    """
    queryset = Call.objects.all()
    serializer_class = CallSerializer


class CallStartListView(generics.ListCreateAPIView):
    """
    get: Lists all unfinished calls
    post: Start a call
    """
    queryset = Call.objects.filter(finished=False)
    serializer_class = CallStartSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,
                       filters.OrderingFilter)
    filter_fields = ('timestamp_start', 'source', 'destination')
    ordering_fields = ('timestamp_start', 'source', 'destination')


class CallStartRetrieveView(generics.RetrieveAPIView):
    """
    get: Details a unfinished call
    """
    queryset = Call.objects.filter(finished=False)
    serializer_class = CallStartSerializer


class CallEndListView(generics.ListCreateAPIView):
    """
    get: List all finished calls
    post: End a call
    """
    queryset = Call.objects.filter(finished=True)
    serializer_class = CallEndSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,
                       filters.OrderingFilter)
    filter_fields = ('timestamp_start', 'timestamp_end', 'source',
                     'destination')
    ordering_fields = ('timestamp_start', 'timestamp_end', 'source',
                       'destination')


class CallEndRetrieveView(generics.RetrieveAPIView):
    """
    get: Details a finished call
    """
    queryset = Call.objects.filter(finished=True)
    serializer_class = CallEndSerializer


class BillView(APIView):
    schema = schemas.AutoSchema(manual_fields=[
        coreapi.Field(
            'source',
            required=True,
            location='path',
            example='55123456789',
            schema=coreschema.String(
                description='Subscriber telephone number')),
        coreapi.Field(
            'year',
            required=False,
            location='query',
            example=2018,
            schema=coreschema.Integer(description='Year period')),
        coreapi.Field(
            'month',
            required=False,
            location='query',
            example=12,
            schema=coreschema.Integer(description='Month period')),
    ])

    def get(self, request, source):
        """
         List all calls finished and yours bills
        """
        year = self.request.query_params.get('year')
        month = self.request.query_params.get('month')
        previous_month = timezone.now().replace(day=1) - timedelta(days=1)
        try:
            year = int(year)
            month = int(month)
            if not self.validate_request_date(year, month):
                return Response(
                    {'error-time': 'Date cant be in future or atual month'},
                    status=400
                )
            queryset = self.get_queryset(source, year, month)
        except (ValueError, TypeError):
            year = previous_month.year
            month = previous_month.month
            queryset = self.get_queryset(source, year, month)

        serializer = CallBillSerializer(
            queryset, many=True, context={
                'request': request
            })
        response_data = {
            'source': source,
            'year': year,
            'month': month,
            'count': queryset.count(),
            'total_bill': Call.get_bill(queryset),
            'call_bills': serializer.data
        }

        return Response(response_data)

    def validate_request_date(self, year, month):
        now = timezone.now()
        if (year, month) >= (now.year, now.month):
            return False
        return True

    def get_queryset(self, source, year, month):
        return Call.objects.filter(
            source=source,
            finished=True,
            timestamp_end__year=year,
            timestamp_end__month=month)
