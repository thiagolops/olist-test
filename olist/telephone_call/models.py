from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models.query import QuerySet
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from .utils import (calculate_minutes_charged, phone_str_representation,
                    validate_phone_number)


class Call(models.Model):
    call_id = models.BigIntegerField(_('Call identifier'), unique=True)
    timestamp_start = models.DateTimeField(
        _('Start record timestamp'), default=timezone.now)
    timestamp_end = models.DateTimeField(_('End record timestamp'), null=True)
    finished = models.BooleanField(default=False, editable=False)

    source = models.CharField(
        _('Origin phone number'),
        validators=[validate_phone_number],
        max_length=11)

    destination = models.CharField(
        _('Destination phone number'),
        validators=[validate_phone_number],
        max_length=11)

    bill = models.FloatField(editable=False, null=True)

    @property
    def source_phone(self):
        return phone_str_representation(self.source)

    @property
    def destination_phone(self):
        return phone_str_representation(self.destination)

    @property
    def is_finished(self):
        return self.finished

    @property
    def call_start_date(self):
        return self.timestamp_start.date()

    @property
    def call_start_time(self):
        return self.timestamp_start.time()

    @property
    def call_duration(self):
        if not self.finished:
            raise Exception('Not finished yet')

        return self.timestamp_end - self.timestamp_start

    @property
    def call_price(self):
        # call not finished can be a price
        if not self.finished:
            raise Exception('Not finished yet')

        return self.bill

    @classmethod
    def end_call(cls, call_id, timestamp_end):
        obj = cls.objects.get(call_id=call_id)

        if timestamp_end < obj.timestamp_start:
            raise ValidationError('Finish must occur after start')

        # change status call
        if not obj.finished:
            obj.timestamp_end = timestamp_end
            obj.finished = True
            if not obj.bill:
                len_calls = calculate_minutes_charged(obj.timestamp_start,
                                                      timestamp_end)
                obj.bill = float(
                    format(len_calls * settings.MINUTE_PRICE + settings.CHARGE,
                           '.2f'))
                obj.save()

        obj.refresh_from_db()
        return obj

    @classmethod
    def get_bill(cls, source):
        if isinstance(source, QuerySet):
            obj = source
        else:
            obj = cls.objects.filter(source=source, finished=True)
        sum_prices = obj.aggregate(models.Sum('bill'))['bill__sum'] or 0
        return float(format(sum_prices, '.2f'))

    def __str__(self):
        return 'Record identifier {}'.format(self.call_id)

    class Meta:
        verbose_name = _('Call')
        verbose_name_plural = _('Calls')
