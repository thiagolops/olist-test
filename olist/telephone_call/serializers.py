from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.reverse import reverse
from rest_framework.validators import UniqueValidator

from .models import Call
from .utils import validate_phone_number


class CallSerializer(serializers.ModelSerializer):
    source = serializers.CharField(validators=[validate_phone_number])
    destination = serializers.CharField(validators=[validate_phone_number])
    timestamp_start = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    timestamp_end = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    links = serializers.SerializerMethodField(
        read_only=True, source='get_links')

    class Meta:
        model = Call
        fields = ('id', 'call_id', 'timestamp_start', 'timestamp_end',
                  'source', 'destination', 'finished', 'bill', 'links')

    def get_links(self, obj):
        request = self.context.get('request')
        return {
            'self': reverse(
                'call-detail', kwargs={'pk': obj.pk}, request=request)
        }


class CallStartSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    call_id = serializers.IntegerField(
        validators=[UniqueValidator(queryset=Call.objects.all())])
    finished = serializers.BooleanField(read_only=True)
    timestamp_start = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    source = serializers.CharField(validators=[validate_phone_number])
    destination = serializers.CharField(validators=[validate_phone_number])

    links = serializers.SerializerMethodField(
        read_only=True, source='get_links')

    def get_links(self, obj):
        request = self.context.get('request')
        return {
            'self':
            reverse(
                'start-call-detail', kwargs={'pk': obj.pk}, request=request)
        }

    def create(self, validated_data):
        call_id = validated_data['call_id']
        timestamp_start = validated_data['timestamp_start']
        source = validated_data['source']
        destination = validated_data['destination']

        return Call.objects.create(
            call_id=call_id,
            timestamp_start=timestamp_start,
            source=source,
            destination=destination)


class CallEndSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    call_id = serializers.IntegerField()
    finished = serializers.BooleanField(read_only=True)
    timestamp_start = serializers.DateTimeField(
        read_only=True, format='%Y-%m-%d %H:%M:%S')
    timestamp_end = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    source = serializers.CharField(read_only=True)
    destination = serializers.CharField(read_only=True)
    links = serializers.SerializerMethodField(
        read_only=True, source='get_links')

    def get_links(self, obj):
        request = self.context.get('request')
        return {
            'self':
            reverse('end-call-detail', kwargs={'pk': obj.pk}, request=request)
        }

    def validate(self, data):
        obj = Call.objects.get(call_id=data['call_id'])
        if obj.timestamp_start >= data['timestamp_end']:
            raise ValidationError('finish must occur after start')

        if obj.is_finished:
            raise ValidationError('call already completed')

        return data

    def validate_call_id(self, value):
        try:
            Call.objects.get(call_id=value)
        except Call.DoesNotExist as exc:
            raise ValidationError(exc)
        return value

    def create(self, validated_data):
        call_id = validated_data['call_id']
        timestamp_end = validated_data['timestamp_end']
        return Call.end_call(call_id=call_id, timestamp_end=timestamp_end)


class CallBillSerializer(CallSerializer):
    call_duration = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Call
        fields = (
            'id', 'call_id', 'timestamp_start', 'timestamp_end', 'source',
            'destination', 'finished', 'bill', 'call_duration', 'links',
        )

    def get_call_duration(self, obj):
        return str(obj.timestamp_end - obj.timestamp_start)
