from datetime import datetime, timedelta

import pytest
from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse
from telephone_call.models import Call

pytestmark = pytest.mark.django_db

call_start_post_invalid_datas = ({
    'call_id': 1,
    'timestamp_start': timezone.now(),
    'source': '0',
    'destination': '1587654321'
}, {
    'call_id': 1,
    'timestamp_start': timezone.now(),
    'source': '14123456789',
    'destination': '0'
}, {
    'call_id': 1,
    'timestamp_start': timezone.now(),
    'source': '14123456789',
    'destination': 'OK'
}, {
    'Call_id': 1,
    'timestamp_start': timezone.now(),
    'source': 'Ok',
    'destination': '151234567890'
})

call_end_post_invalid_datas = (
    {
        'call_id': 1,
        'timestamp_end': timezone.make_aware(datetime(2015, 3, 20, 17, 0, 0))
    },
    {
        'call_id': 3,
        'timestamp_end': timezone.make_aware(datetime(2015, 3, 20, 19, 0, 0))
    },
)


def test_view_call_start_200(call, client):
    url = reverse('start-call-list')
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK


def test_view_call_start_list_one(call, client):
    data = {
        'id': call.id,
        'call_id': 1,
        'finished': False,
        'timestamp_start': call.timestamp_start.strftime('%Y-%m-%d %H:%M:%S'),
        'source': '14123456789',
        'destination': '1587654321',
        'links': {
            'self': 'http://testserver/api/calls/start/{}/'.format(call.id)
        }
    }

    url = reverse('start-call-list')
    response = client.get(url)
    assert response.json() == [
        data,
    ]


def test_view_call_start_list_many(many_calls_no_end, many_calls, client):
    url = reverse('start-call-list')
    response = client.get(url)
    assert len(response.json()) == 15


def test_view_call_start_post(client):
    url = reverse('start-call-list')
    data = {
        'call_id': 1,
        'timestamp_start': timezone.now(),
        'source': '14123456789',
        'destination': '1587654321'
    }
    response = client.post(url, data, format='json')
    assert response.status_code == status.HTTP_201_CREATED


def test_view_call_start_post_same_id(client):
    url = reverse('start-call-list')
    data = {
        'call_id': 1,
        'timestamp_start': timezone.now(),
        'source': '14123456789',
        'destination': '1587654321'
    }
    client.post(url, data, format='json')
    response = client.post(url, data, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.parametrize('data', call_start_post_invalid_datas)
def test_view_call_start_post_invalid_source(client, data):
    url = reverse('start-call-list')
    response = client.post(url, data, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_view_call_start_detail_status_code(client, call):
    url = reverse('start-call-detail', kwargs={'pk': call.id})
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK


def test_view_call_start_detail_json(client, call):
    data = {
        'id': call.id,
        'call_id': 1,
        'finished': False,
        'timestamp_start': call.timestamp_start.strftime('%Y-%m-%d %H:%M:%S'),
        'source': '14123456789',
        'destination': '1587654321',
        'links': {
            'self': 'http://testserver/api/calls/start/{}/'.format(call.id)
        }
    }
    url = reverse('start-call-detail', kwargs={'pk': call.id})
    response = client.get(url)
    assert response.json() == data


def test_view_call_start_detail_404(client, call):
    url = reverse('start-call-detail', kwargs={'pk': call.id + 1})
    response = client.get(url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_view_call_end_200(bill, client):
    url = reverse('end-call-list')
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK


def test_view_call_end_one(bill, client):
    data = {
        'id': bill.id,
        'call_id': 1,
        'finished': True,
        'timestamp_start': bill.timestamp_start.strftime('%Y-%m-%d %H:%M:%S'),
        'timestamp_end': bill.timestamp_end.strftime('%Y-%m-%d %H:%M:%S'),
        'source': '14123456789',
        'destination': '1587654321',
        'links': {
            'self': 'http://testserver/api/calls/end/{}/'.format(bill.id)
        }
    }
    url = reverse('end-call-list')
    response = client.get(url)

    assert response.json() == [
        data,
    ]


def test_view_call_end_list_many(many_calls_no_end, many_calls, client):
    url = reverse('end-call-list')
    response = client.get(url)

    assert len(response.json()) == 15


def test_view_call_end_post(client, call):
    data = {
        'call_id': 1,
        'timestamp_end': timezone.now() + timedelta(minutes=10)
    }
    url = reverse('end-call-list')
    response = client.post(url, data)

    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.parametrize('data', call_end_post_invalid_datas)
def test_view_call_end_invalid(client, data, call):
    url = reverse('end-call-list')
    response = client.post(url, data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_view_call_end_already_completed(client, bill):
    url = reverse('end-call-list')
    data = {
        'call_id': 1,
        'timestamp_end': timezone.now() + timedelta(minutes=10)
    }
    response = client.post(url, data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()['non_field_errors'] == [
        'call already completed',
    ]


def test_view_call_end_detail_status_code(client, bill):
    url = reverse('end-call-detail', kwargs={'pk': bill.id})
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK


def test_view_call_end_detail_data(client, bill):
    data = {
        'id': bill.id,
        'call_id': 1,
        'finished': True,
        'timestamp_start': bill.timestamp_start.strftime('%Y-%m-%d %H:%M:%S'),
        'timestamp_end': bill.timestamp_end.strftime('%Y-%m-%d %H:%M:%S'),
        'source': '14123456789',
        'destination': '1587654321',
        'links': {
            'self': 'http://testserver/api/calls/end/{}/'.format(bill.id)
        }
    }
    url = reverse('end-call-detail', kwargs={'pk': bill.id})
    response = client.get(url)

    assert response.json() == data


def test_view_calls_status_code(client, bill):
    url = reverse('call-list')
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK


def test_view_calls_response_is_a_list(client, bill):
    url = reverse('call-list')
    response = client.get(url)

    assert isinstance(response.json(), list)


def test_view_calls_response_item(client, bill):
    data = {
        'id': bill.id,
        'call_id': 1,
        'timestamp_start': bill.timestamp_start.strftime('%Y-%m-%d %H:%M:%S'),
        'timestamp_end': bill.timestamp_end.strftime('%Y-%m-%d %H:%M:%S'),
        'source': '14123456789',
        'destination': '1587654321',
        'finished': True,
        'bill': bill.bill,
        'links': {
            'self': 'http://testserver/api/calls/{}/'.format(bill.id)
        },
    }
    url = reverse('call-detail', kwargs={'pk': bill.id})
    response = client.get(url)

    assert response.json() == data


def test_view_calls_delete(client, bill):
    url = reverse('call-detail', kwargs={'pk': bill.id})
    response = client.delete(url)

    assert response.status_code == status.HTTP_204_NO_CONTENT


def test_view_bill_status_code(client, bill):
    url = reverse('bill-list', kwargs={'source': '14123456789'})
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK


def test_view_bill_response(client, bill):
    year = bill.timestamp_start.year
    month = bill.timestamp_start.month
    data = {
        'source': 14123456789,
        'year': year,
        'month': month,
        'count': 1,
        'total_bill': 0.54,
        'call_bills': [{
            'id': bill.id,
            'call_id': 1,
            'timestamp_start': bill.timestamp_start.strftime(
                '%Y-%m-%d %H:%M:%S'),
            'timestamp_end': bill.timestamp_end.strftime(
                '%Y-%m-%d %H:%M:%S'),
            'source': '14123456789',
            'destination': '1587654321',
            'finished': True,
            'bill': 0.54,
            'call_duration': '0:02:00',
            'links': {
                'self': 'http://testserver/api/calls/{}/'.format(bill.id)
            }
        }]
    }  # yapf: disable
    url = reverse(
        'bill-list', kwargs={
            'source': '14123456789',
        })
    response = client.get(url, {
        'year': year,
        'month': month,
    })

    assert response.json() == data


def test_view_bill_last_month(client):
    past = timezone.now().replace(day=1) - timedelta(days=1)
    url = reverse('bill-list', kwargs={'source': '14123456789'})
    request = client.get(url)
    json = request.json()

    assert json['month'] == past.month
    assert json['year'] == past.year


def test_view_bill_len_with_more_calls(client, many_calls):
    previous = timezone.now().replace(day=1) - timedelta(days=1)
    url = reverse('bill-list', kwargs={'source': '14123456789'})
    request = client.get(url, {'year': previous.year, 'month': previous.month})
    json = request.json()

    assert len(json['call_bills']) == 15


def test_view_bill_with_more_calls(client, many_calls):
    previous = timezone.now().replace(day=1) - timedelta(days=1)
    url = reverse('bill-list', kwargs={'source': '14123456789'})
    request = client.get(url, {'year': previous.year, 'month': previous.month})
    json = request.json()

    assert json['total_bill'] == 102.60


def test_view_invalid_present_month(client, many_calls):
    today = timezone.now()
    url = reverse('bill-list', kwargs={'source': '14123456789'})
    request = client.get(url, {'year': today.year, 'month': today.month})

    assert request.status_code == status.HTTP_400_BAD_REQUEST


def test_view_invalid_future_month(client, many_calls):
    today = timezone.now() + timedelta(weeks=1)
    url = reverse('bill-list', kwargs={'source': '14123456789'})
    request = client.get(url, {'year': today.year, 'month': today.month})

    assert request.status_code == status.HTTP_400_BAD_REQUEST


def test_view_bill_end_present_month(client):
    previous = timezone.now().replace(
        day=1, hour=23, minute=0, second=0) - timedelta(days=1)
    end = previous + timedelta(hours=2)
    Call.objects.create(
        call_id=1,
        timestamp_start=previous,
        source='14123456789',
        destination='1587654321')
    Call.end_call(call_id=1, timestamp_end=end)

    url = reverse('bill-list', kwargs={'source': '14123456789'})
    request_previus = client.get(url, {
        'year': previous.year,
        'month': previous.month
    })

    assert len(request_previus.json()['call_bills']) == 0


def test_view_bill_end_present_with_many(client, many_calls):
    previous = timezone.now().replace(
        day=1, hour=23, minute=0, second=0) - timedelta(days=1)
    end = previous + timedelta(hours=2)
    Call.objects.create(
        call_id=999,
        timestamp_start=previous,
        source='14123456789',
        destination='1587654321')
    Call.end_call(call_id=999, timestamp_end=end)

    url = reverse('bill-list', kwargs={'source': '14123456789'})
    request_previus = client.get(url)

    assert len(request_previus.json()['call_bills']) == 15
    assert request_previus.json()['total_bill'] == 102.60
