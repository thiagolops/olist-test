from datetime import datetime, timedelta

import pytest
from django.utils import timezone
from rest_framework.reverse import reverse

from telephone_call.models import Call
from telephone_call.serializers import CallEndSerializer, CallStartSerializer

pytestmark = pytest.mark.django_db


def test_call_start_serializator(call):
    serializer_output = {
        'id': call.id,
        'call_id': 1,
        'finished': False,
        'timestamp_start': call.timestamp_start.strftime('%Y-%m-%d %H:%M:%S'),
        'source': '14123456789',
        'destination': '1587654321',
        'links': {
            'self': reverse('start-call-detail', kwargs={
                'pk': call.id
            })
        },
    }

    serializer = CallStartSerializer(call)
    assert serializer.data == serializer_output


def test_call_start_serializator_valid():
    time_creation = timezone.now()
    serializer_input = {
        'call_id': 1,
        'timestamp_start': time_creation,
        'source': '14123456789',
        'destination': '1212345678'
    }
    serializer = CallStartSerializer(data=serializer_input)
    assert serializer.is_valid()


def test_call_start_serializator_invalid():
    time_creation = timezone.now()
    serializer_input = {
        'call_id': 1,
        'timestamp_start': time_creation,
        'source': '1',
        'destination': '1212345678'
    }
    serializer = CallStartSerializer(data=serializer_input)
    assert not serializer.is_valid()


def test_call_start_serializator_create():
    time_creation = timezone.now()
    serializer_input = {
        'call_id': 1,
        'timestamp_start': time_creation,
        'source': '14123456789',
        'destination': '1212345678'
    }
    serializer = CallStartSerializer(data=serializer_input)
    serializer.is_valid()
    assert isinstance(serializer.save(), Call)


def test_call_start_serializator_unique_call_id(call):
    time_creation = timezone.now()
    serializer_input = {
        'call_id': 1,
        'timestamp_start': time_creation,
        'source': '14123456789',
        'destination': '1212345678'
    }
    serializer = CallStartSerializer(data=serializer_input)
    assert not serializer.is_valid()


def test_call_end_serializator(bill):
    serializer_output = {
        'id': bill.id,
        'call_id': 1,
        'timestamp_start': bill.timestamp_start.strftime('%Y-%m-%d %H:%M:%S'),
        'timestamp_end': bill.timestamp_end.strftime('%Y-%m-%d %H:%M:%S'),
        'source': '14123456789',
        'destination': '1587654321',
        'finished': True,
        'links': {
            'self': reverse('end-call-detail', kwargs={
                'pk': bill.pk
            })
        },
    }
    serializer = CallEndSerializer(bill)

    assert serializer.data == serializer_output


def test_call_end_valid(call):
    time_creation = timezone.now() + timedelta(minutes=10)
    serializer_input = {'call_id': 1, 'timestamp_end': time_creation}
    serializer = CallEndSerializer(data=serializer_input)

    assert serializer.is_valid()


def test_call_end_invalid_past(call):
    time_past = timezone.make_aware(datetime(2017, 3, 20, 17, 0, 13))
    serializer_input = {'call_id': 1, 'timestamp_end': time_past}
    serializer = CallEndSerializer(data=serializer_input)

    assert not serializer.is_valid()


def test_call_end_invalid_call_id_not_exist():
    time_creation = timezone.now() + timedelta(minutes=10)
    serializer_input = {'call_id': 2, 'timestamp_end': time_creation}
    serializer = CallEndSerializer(data=serializer_input)

    assert serializer.is_valid


def test_call_end_all_invalid():
    time_creation = timezone.now() - timedelta(minutes=10)
    serializer_input = {'call_id': 2, 'timestamp_end': time_creation}
    serializer = CallEndSerializer(data=serializer_input)

    assert serializer.is_valid
