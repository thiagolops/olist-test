from datetime import date, datetime, time, timedelta

import pytest
from django.core.exceptions import ValidationError
from django.utils import timezone

from telephone_call.models import Call, validate_phone_number

pytestmark = pytest.mark.django_db

test_data_price = (
    (timezone.make_aware(datetime(2018, 3, 20, 21, 57, 13)),
     timezone.make_aware(datetime(2018, 3, 21, 6, 10, 56)), 1.53),
    (timezone.make_aware(datetime(2018, 3, 20, 21, 57, 13)),
     timezone.make_aware(datetime(2018, 3, 20, 22, 10, 56)), 0.54),
    (timezone.make_aware(datetime(2018, 3, 20, 18, 0, 13)),
     timezone.make_aware(datetime(2018, 3, 20, 19, 13, 12)), 6.84),
    (timezone.make_aware(datetime(2018, 2, 20, 21, 57, 13)),
     timezone.make_aware(datetime(2018, 2, 21, 6, 00, 10)), 0.54),
    (timezone.make_aware(datetime(2018, 2, 20, 18, 00, 00)),
     timezone.make_aware(datetime(2018, 2, 20, 19, 14, 58)), 7.02),
    (timezone.make_aware(datetime(2017, 12, 12, 15, 7, 13)),
     timezone.make_aware(datetime(2017, 12, 12, 15, 14, 56)), 0.99),
    (timezone.make_aware(datetime(2017, 12, 12, 4, 57, 13)),
     timezone.make_aware(datetime(2017, 12, 12, 6, 10, 56)), 1.26),
    (timezone.make_aware(datetime(2017, 12, 12, 21, 57, 13)),
     timezone.make_aware(datetime(2017, 12, 13, 22, 10, 56)), 86.94),
    (timezone.make_aware(datetime(2017, 12, 12, 21, 57, 00)),
     timezone.make_aware(datetime(2017, 12, 13, 6, 0, 0)), 0.63),
    (timezone.make_aware(datetime(2017, 12, 12, 6, 0, 0)),
     timezone.make_aware(datetime(2017, 12, 12, 6, 1, 0)), 0.45),
    (timezone.make_aware(datetime(2017, 12, 12, 18, 0, 0)),
     timezone.make_aware(datetime(2017, 12, 12, 18, 2, 0)), 0.54),
    (timezone.make_aware(datetime(2017, 12, 12, 5, 0, 13)),
     timezone.make_aware(datetime(2017, 12, 12, 6, 1, 13)), 0.45),
)


def test_count_create_call(call):
    assert Call.objects.count() == 1


def test_source_phone_in_call(call):
    assert call.source_phone == '(14) 12345-6789'


def test_destination_phone_in_call(call):
    assert call.destination_phone == '(15) 8765-4321'


def test_has_end_call_attribute(call):
    assert hasattr(Call, 'end_call')


def test_call_is_finished(call):
    assert not call.is_finished


def test_end_call(call):
    time_call = timezone.now() + timedelta(hours=1)
    Call.end_call(call_id=1, timestamp_end=time_call)
    call.refresh_from_db()
    assert call.is_finished


def test_end_call_without_call():
    time_call = timezone.now() + timedelta(hours=1)
    with pytest.raises(Call.DoesNotExist):
        Call.end_call(call_id=2, timestamp_end=time_call)


def test_end_call_in_past(call):
    time_call = timezone.make_aware(datetime(2016, 3, 20, 17, 0, 13))
    with pytest.raises(ValidationError) as exc:
        Call.end_call(call_id=1, timestamp_end=time_call)
        assert 'Phone calls can not be negative' == str(exc)


def test_call_has_bill():
    assert hasattr(Call, 'get_bill')


def test_call_get_bill_without_calls():
    assert Call.get_bill(source='14123456789') == 0


def test_call_has_start_date():
    assert hasattr(Call, 'call_start_date')


def test_call_start_date_return(call):
    assert isinstance(call.call_start_date, date)


def test_call_has_start_time():
    assert hasattr(Call, 'call_start_time')


def test_call_start_time_return(call):
    assert isinstance(call.call_start_time, time)


def test_call_has_call_duration():
    assert hasattr(Call, 'call_duration')


def test_call_call_duration(bill):
    assert isinstance(bill.call_duration, timedelta)


def test_call_has_call_price():
    assert hasattr(Call, 'call_price')


def test_call_price_without_end_call(call):
    with pytest.raises(Exception):
        call.call_price()


def test_call_price(bill):
    assert isinstance(bill.call_price, float)


@pytest.mark.parametrize('time_start, time_end, result', test_data_price)
def test_call_price_value_dawn(time_start, time_end, result):
    Call.objects.create(
        call_id=1,
        timestamp_start=time_start,
        source='14123456789',
        destination='1587654321')

    Call.end_call(call_id=1, timestamp_end=time_end)
    call = Call.objects.get(call_id=1)
    assert call.call_price == result


def test_validator_phone_number_less():
    with pytest.raises(ValidationError):
        validate_phone_number('141234567')


def test_validator_phone_number_more():
    with pytest.raises(ValidationError):
        validate_phone_number('151234567890')


def test_validator_phone_number_eight():
    assert validate_phone_number('1412345678') is None


def test_validator_phone_number_nine():
    assert validate_phone_number('14123456789') is None


def test_call_get_bill_with_calls(many_calls):
    source = '14123456789'
    assert Call.get_bill(source=source) == 102.60
