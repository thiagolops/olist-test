from datetime import datetime, timedelta

import pytest
from django.utils import timezone
from model_mommy import mommy
from rest_framework.test import APIClient

from telephone_call.models import Call


@pytest.fixture
def call():
    time = timezone.now().replace(day=1, hour=12) - timedelta(days=1)
    call = Call.objects.create(
        call_id=1,
        timestamp_start=time,
        source='14123456789',
        destination='1587654321')
    return call


@pytest.fixture
def bill():
    time_start = timezone.now().replace(day=1, hour=12) - timedelta(days=1)
    time_end = time_start + timedelta(minutes=2)

    call = Call.objects.create(
        call_id=1,
        timestamp_start=time_start,
        source='14123456789',
        destination='1587654321')

    Call.end_call(call_id=1, timestamp_end=time_end)
    call.refresh_from_db()
    return call


@pytest.fixture
def many_calls():
    source = '14123456789'
    time_start = timezone.now().replace(day=1, hour=12) - timedelta(days=1)
    time_end = time_start + timedelta(hours=1, minutes=12, seconds=59)

    for id in range(15):
        mommy.make(Call, timestamp_start=time_start, call_id=id, source=source)
        Call.end_call(call_id=id, timestamp_end=time_end)


@pytest.fixture
def many_calls_no_end():
    for id in range(15):
        mommy.make(Call)


@pytest.fixture
def client():
    client = APIClient()
    return client
