from datetime import timedelta

from django.core.exceptions import ValidationError


def validate_phone_number(number):
    len_number = len(number)
    if len_number not in (10, 11):
        raise ValidationError('{} is not a valid phone number'.format(number))


def timedelta_minutes(start, end):
    return round((end - start).total_seconds() / 60)


def phone_str_representation(number):
    area, first, second = number[:2], number[2:-4], number[-4:]
    return '({}) {}-{}'.format(area, first, second)


def iter_date(start, end):
    while True:
        if start.hour < 22 and start.hour >= 6:
            if start > end:
                break
            yield start
        start = start + timedelta(minutes=1)


def calculate_minutes_charged(start, end):
    date_list = iter_date(start, end)
    number_dates = sum(1 for _ in date_list) - 1
    return 0 if number_dates == -1 else number_dates
