from django.urls import path

from .views import (BillView, CallEndListView, CallEndRetrieveView,
                    CallListView, CallRetrieve, CallStartListView,
                    CallStartRetrieveView)

urlpatterns = [
    path('calls/', CallListView.as_view(), name='call-list'),
    path('calls/<int:pk>/', CallRetrieve.as_view(), name='call-detail'),
    path('calls/start/', CallStartListView.as_view(), name='start-call-list'),
    path(
        'calls/start/<int:pk>/',
        CallStartRetrieveView.as_view(),
        name='start-call-detail'),
    path('calls/end/', CallEndListView.as_view(), name='end-call-list'),
    path(
        'calls/end/<int:pk>/',
        CallEndRetrieveView.as_view(),
        name='end-call-detail'),
    path('bill/<int:source>/', BillView.as_view(), name='bill-list')
]
