from django.apps import AppConfig


class TelephoneCallConfig(AppConfig):
    name = 'telephone_call'
