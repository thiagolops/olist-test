# Generated by Django 2.0.3 on 2018-03-29 20:46

from django.db import migrations, models
import telephone_call.utils


class Migration(migrations.Migration):

    dependencies = [
        ('telephone_call', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='call',
            name='destination',
            field=models.CharField(max_length=11, validators=[telephone_call.utils.validate_phone_number], verbose_name='Destination phone number'),
        ),
        migrations.AlterField(
            model_name='call',
            name='source',
            field=models.CharField(max_length=11, validators=[telephone_call.utils.validate_phone_number], verbose_name='Origin phone number'),
        ),
    ]
