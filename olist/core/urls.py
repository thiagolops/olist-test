from django.contrib import admin
from django.urls import include, path
from rest_framework_swagger.views import get_swagger_view

from telephone_call.urls import urlpatterns

schema_view = get_swagger_view(title='API Calls')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(urlpatterns)),
    path('', schema_view)
]
