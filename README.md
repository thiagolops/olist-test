# Telephone call API ☎

## About

Telephone call API is an application then prove a interface to receives calls records, some that's flow is: **start one call record**, **end a call**, calculate and check a **monthly bill** for a given a telephone number.

The features are:

* Start call record
* End a call record
* List all calls
* Check a bill

## Requirements

* Python 3.6.x
* [Pipenv](https://docs.pipenv.org/)
* [Postgres](https://postgresql.org) or [SQLITE](https://sqlite.org/index.html)

## Installing

* Configure your virtual environment, you can use `virtualenv` or `pipenv` to do this.
* After your environment configured, install the `pipenv`: `pip install pipenv`
* Install requiriments: `pipenv install`
* Install tests requiriments: `pipenv install --dev`

* Enter project folder: `cd olist`
* Create a file called `.env` with some configs:
  - `SECRET_KEY`: your secret key
  - `DATABASE_URL`: url to access database. postgres example: `postgres://USERNAME:PASSWORD@IP:PORT/DBNAME`
  - `DEBUG`: True or False, default is False **(OPTIONAL)**
* If debug is False don't forget run collectstatic: ` python manage.py collectstatic`
* Run migrate: `python manage.py migrate`
* RUN!: `python manage.py runserver`

## Testing

* After requirements installed and migrate ran, just run pytest: `pytest`


## Documentation

This project use **Swagger** to document API, just access [http://localhost:8000/](http://localhost:8000) to receive the docs.


### 1. Receive start telephone call records

To start a call use a endpoint: `/api/calls/start`

#### GET

##### List
Endpoint: `/api/calls/start`

Code 200.
Content list with started calls, example:

```
{
    "id": , // Record unique indentificator;
    "call_id": , // Unique for each call record pair;
    "finished": , // Inidicate if is finished with true or false;
    "timestamp_start": , // The timestamp of when call start;
    "source": , // The subscriber phone number that originated the call;
    "destination": , // The phone number receiveing the call;
    "links": {
        "self": , // URL to call
    }
}
```

##### Detail
Endpoint: `/api/calls/start/:id`

Code: 200.
Receive a unique call started passing a `:id`

#### POST
Endpoint: `/api/calls/start`

Code: 201.
Content: Create a call start and return with same structure Detail GET.

POST example:

```
{
    "call_id":  // Unique for each call record pair;
    "timestamp_start": , // The timestamp of when call start;
    "source":  // The subscriber phone number that originated the call;
    "destination":  // The phone number receiving the call.
}
```

### 2. Receive end telephone call records

To end a call use a endpoint: `/api/calls/end`

#### GET

##### List
Endpoint: `/api/calls/start`

Code: 200.
Content: list with ended calls, example:

```
{
    "id": , // Record unique indentificator;
    "call_id": , // Unique for each call record pair;
    "finished": , // Inidicate if is finished with true or false;
    "timestamp_start": , // The timestamp of when call start;
    "timestamp_end": , // The timestamp of when call end;
    "source": , // The subscriber phone number that originated the call;
    "destination": , // The phone number receiveing the call;
    "links": {
        "self": , // URL to call
    }
}
```

##### Detail
Endpoint: `/api/calls/end/:id`

Code: 200.
Receive a unique call ended passing a `:id`

#### POST
Create a end call, when already ended and price of this call calculed, can't be changed.

Endpoint: `/api/calls/end`
Code: 201
Content: Unique call with same strutcture in GET

POST example:
```
{
    "call_id":  // Unique for each call record pair;
    "timestamp_end": // The Timestamp of when call end;
}
```

### 3. Calls records
To see calls use a endpoint: /api/calls

#### GET

##### List
Endpoint: `/api/calls/`

Code: 200
Content: list with all calls.
Response example:
```
{
    "id": , // Record unique indentificator;
    "call_id": , // Unique for each call record pair;
    "finished": , // Inidicate if is finished with true or false;
    "timestamp_start": , // The timestamp of when call start;
    "timestamp_end": , // The timestamp of when call end;
    "source": , // The subscriber phone number that originated the call;
    "destination": , // The phone number receiveing the call;
    "bill": , // if finished the price of this call;
    "links": {
        "self": // URL to call
    }
}
```

##### Detail
Receive a unique call passing a `:id`
Endpoint: `/api/calls/:id`

Code: 200
Content: Reponse with same structure in GET

#### DELETE
Delete a call passing a `:id`
Endpoint; `/api/calls/:id`

Code: 204

### 4. Monthly bill

#### Get

To get a bill use endpoint `/api/bill/:source`

Parameter required

- `source`: is a telephone number of the subscriber that originated the calls

Parameter optional

- `year` and `month`: represent a period bill, if the period is not informed will be considered last month.

Code: 200
Content:
```
{
    "source": // The subscriber phone number
    "year": // Year
    "month": // Month
    "count": // Number calls
    "total_bill": // Total price
    "call_bills": // list with all bills
    [
        {
            "id": , // Record unique indentificator;
            "call_id": , // Unique for each call record pair;
            "finished": , // Inidicate if is finished with true or false;
            "timestamp_start": , // The timestamp of when call start;
            "timestamp_end": , // The timestamp of when call end;
            "source": , // The subscriber phone number that originated the call;
            "destination": , // The phone number receiveing the call;
            "bill": , // if finished the price of this call;
            "call_duration": // Call duration
            "links": {
                "self": // URL to call
            }
        }
    ]
}
```

## Enviroment used in this project

* OS: Debian GNU/Linux testing (buster) x86_64
* Host: DELL Inspiron 5458
* Editor: EMACS 25 - configs [here](https://github.com/thiagolopes/emacs)
* Python 3.6.5
* Virtualenv wrapper
* Pipenv
* SQLITE and Postgres
* Django, DRF to RESTAPI, docs with Swagger
* Test with pytest
